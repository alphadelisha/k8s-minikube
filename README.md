# Kubernetes deployment manifest for pingpong-app


# How to use

## Build pingpong-app docker image

1. Clone pingpong-app repo into your local machine
- git clone https://gitlab.com/alphadelisha/pingpong-app.git

2. Open pingpong-app and Set env file
- cd pingpong-app && ./setenv.sh

3. Build image from pingpong-app
- docker build -t pingpong-server .

## Deploy app with k8s-minikube

4. Clone this repo into your local machine
- git clone https://gitlab.com/alphadelisha/k8s-minikube.git

5. Open k8s-minikube folder that you already clone

6. Deploy pingpong-app
- kubectl create -f pingpong-app.yml

7. Make sure the deployment success, up and running
- kubectl get deployments

8. Create service for pingpong-app
- kubectl apply -f pingpong-svc.yml

9. Get service access url
- minikube service pingpong-app --url

10. Test deployment with curl followed by url who get from step 9




